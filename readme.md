- script will make icons in current folder for pwa sizes: 36 48 60 72 76 96 120 152 180 192 512
- original file must be caled icon.png (won't be deleted after run)
- one of the output files will be icon-192x192.png
- will run convert command so you need image magic:
https://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/

Tu use:
```
$ ./png_to_pwa.sh
```
