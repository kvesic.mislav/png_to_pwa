formats=( 36 48 60 72 76 96 120 152 180 192 512 )
for i in "${formats[@]}"
do
  convert icon.png -resize "$i"x"$i" icon-"$i"x"$i".png
done 
echo "Done"
